<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* ROUTES FROM REST API WHIT LARAVEL */
//Rutas publicas
Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
//Rutas protejidas JWT
Route::group(['middleware' => 'auth.jwt'], function () {
    //Rutas de sesión de usuario
    Route::get('logout', 'ApiController@logout');
    Route::get('user', 'ApiController@getAuthUser');
});
